class Afiliacion {
  String sId;
  dynamic firmaAfiliacion;
  String nombre;
  String tipoDocumento;
  String numeroDocumento;
  String direccion;
  String telefonoFijo;
  String telefonoMovil;
  String correo;
  String edad;
  String ciudad;
  bool directivo;
  int iV;

  Afiliacion(
      {this.sId,
      this.firmaAfiliacion,
      this.nombre,
      this.tipoDocumento,
      this.numeroDocumento,
      this.direccion,
      this.telefonoFijo,
      this.telefonoMovil,
      this.correo,
      this.edad,
      this.ciudad,
      this.directivo,
      this.iV});

  Afiliacion.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    firmaAfiliacion = json['firmaAfiliacion'];
    nombre = json['nombre'];
    tipoDocumento = json['tipoDocumento'];
    numeroDocumento = json['numeroDocumento'];
    direccion = json['direccion'];
    telefonoFijo = json['telefonoFijo'];
    telefonoMovil = json['telefonoMovil'];
    correo = json['correo'];
    edad = json['edad'];
    ciudad = json['ciudad'];
    iV = json['__v'];
    if (json['directivo'] != null) {
      directivo = json['directivo'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['firmaAfiliacion'] = this.firmaAfiliacion;
    data['nombre'] = this.nombre;
    data['tipoDocumento'] = this.tipoDocumento;
    data['numeroDocumento'] = this.numeroDocumento;
    data['direccion'] = this.direccion;
    data['telefonoFijo'] = this.telefonoFijo;
    data['telefonoMovil'] = this.telefonoMovil;
    data['correo'] = this.correo;
    data['edad'] = this.edad;
    data['ciudad'] = this.ciudad;
    data['__v'] = this.iV;
    data['directivo'] = this.directivo;
    return data;
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map['_id'] = sId;
    map['firmaAfiliacion'] = firmaAfiliacion;
    map['nombre'] = nombre;
    map['tipoDocumento'] = tipoDocumento;
    map['numeroDocumento'] = numeroDocumento;
    map['direccion'] = direccion;
    map['telefonoFijo'] = telefonoFijo;
    map['telefonoMovil'] = telefonoMovil;
    map['correo'] = correo;
    map['edad'] = edad;
    map['ciudad'] = ciudad;
    map['directivo'] = directivo;
    map['__v'] = iV;
    return map;
  }
}
