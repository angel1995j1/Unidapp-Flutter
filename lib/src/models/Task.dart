class Tasks {
  List<Tareas> tareas;

  Tasks({this.tareas});

  Tasks.fromJson(Map<String, dynamic> json) {
    if (json['tareas'] != null) {
      tareas = new List<Tareas>();
      json['tareas'].forEach((v) {
        tareas.add(new Tareas.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.tareas != null) {
      data['tareas'] = this.tareas.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Tareas {
  List<String> usuario;
  bool finalizado;
  String sId;
  String asignado;
  String titulo;
  String tarea;
  String creado;
  String fechaLimite;
  List<Subtarea> subtarea;
  String createdAt;
  int iV;

  Tareas(
      {this.usuario,
      this.finalizado,
      this.sId,
      this.asignado,
      this.titulo,
      this.tarea,
      this.creado,
      this.fechaLimite,
      this.subtarea,
      this.createdAt,
      this.iV});

  Tareas.fromJson(Map<String, dynamic> json) {
    usuario = json['usuario'].cast<String>();
    finalizado = json['finalizado'];
    sId = json['_id'];
    asignado = json['asignado'];
    titulo = json['titulo'];
    tarea = json['tarea'];
    creado = json['creado'];
    fechaLimite = json['fechaLimite'];
    if (json['subtarea'] != null) {
      subtarea = new List<Subtarea>();
      json['subtarea'].forEach((v) {
        subtarea.add(new Subtarea.fromJson(v));
      });
    }
    createdAt = json['created_at'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['usuario'] = this.usuario;
    data['finalizado'] = this.finalizado;
    data['_id'] = this.sId;
    data['asignado'] = this.asignado;
    data['titulo'] = this.titulo;
    data['tarea'] = this.tarea;
    data['creado'] = this.creado;
    data['fechaLimite'] = this.fechaLimite;
    if (this.subtarea != null) {
      data['subtarea'] = this.subtarea.map((v) => v.toJson()).toList();
    }
    data['created_at'] = this.createdAt;
    data['__v'] = this.iV;
    return data;
  }
}

class Subtarea {
  bool finalizar;
  String sId;
  String titulo;

  Subtarea({this.finalizar, this.sId, this.titulo});

  Subtarea.fromJson(Map<String, dynamic> json) {
    finalizar = json['finalizar'];
    sId = json['_id'];
    titulo = json['titulo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['finalizar'] = this.finalizar;
    data['_id'] = this.sId;
    data['titulo'] = this.titulo;
    return data;
  }
}
