class TareaDetalle {
  TareaDetalle({
    this.terminada,
    this.finalizado,
    this.id,
    this.asignado,
    this.titulo,
    this.tarea,
    this.usuario,
    this.creado,
    this.fechaLimite,
    this.subtarea,
    this.v,
  });

  bool terminada;
  String finalizado;
  String id;
  String asignado;
  String titulo;
  String tarea;
  String usuario;
  String creado;
  String fechaLimite;
  List<Subtarea> subtarea;
  int v;

  factory TareaDetalle.fromJson(Map<String, dynamic> json) => TareaDetalle(
        terminada: json["terminada"],
        finalizado: json["finalizado"],
        id: json["_id"],
        asignado: json["asignado"],
        titulo: json["titulo"],
        tarea: json["tarea"],
        usuario: json["usuario"],
        creado: json["creado"],
        fechaLimite: json["fechaLimite"],
        subtarea: List<Subtarea>.from(
            json["subtarea"].map((x) => Subtarea.fromJson(x))),
        v: json["__v"],
      );

  Map<String, dynamic> toJson() => {
        "terminada": terminada,
        "finalizado": finalizado,
        "_id": id,
        "asignado": asignado,
        "titulo": titulo,
        "tarea": tarea,
        "usuario": usuario,
        "creado": creado,
        "fechaLimite": fechaLimite,
        "subtarea": List<dynamic>.from(subtarea.map((x) => x.toJson())),
        "__v": v,
      };
}

class Subtarea {
  Subtarea({
    this.finalizar,
    this.id,
    this.titulo,
  });

  bool finalizar;
  String id;
  String titulo;

  factory Subtarea.fromJson(Map<String, dynamic> json) => Subtarea(
        finalizar: json["finalizar"],
        id: json["_id"],
        titulo: json["titulo"],
      );

  Map<String, dynamic> toJson() => {
        "finalizar": finalizar,
        "_id": id,
        "titulo": titulo,
      };
}
