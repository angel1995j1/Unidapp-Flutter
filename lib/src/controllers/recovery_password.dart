import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../api/user_repository.dart' as userRepository;

class RecoveryPasswordController extends ControllerMVC {
  String email;
  bool loading = false;

  GlobalKey<FormState> recoveryFormKey;
  GlobalKey<ScaffoldState> scaffoldKey;

  RecoveryPasswordController() {
    this.scaffoldKey = GlobalKey<ScaffoldState>();
    this.recoveryFormKey = GlobalKey<FormState>();
  }

  void sendRecoveryEmail() {
    loading = true;
    if (recoveryFormKey.currentState.validate()) {
      recoveryFormKey.currentState.save();
      userRepository
          .sendEmailRecoveryPassword(email: email)
          .then((res) => {
                scaffoldKey?.currentState?.showSnackBar(SnackBar(
                  content: Text(res),
                ))
              })
          .catchError((e) {
        print('catch error $e');
      }).whenComplete(() => {
                print('On complete'),
                setState(() {
                  loading = false;
                })
              });
    }
  }
}
