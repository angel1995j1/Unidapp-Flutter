import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:unidapp/src/api/user_repository.dart';
import 'package:unidapp/src/controllers/contenidosFormacion_controller.dart';
import 'package:unidapp/src/elements/CircularLoadingWidget.dart';
import 'package:unidapp/src/elements/DrawerWidget.dart';
import 'package:unidapp/src/elements/PermisosDenegadosWidget.dart';

class ContenidosFormacion extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  ContenidosFormacion({Key key, this.parentScaffoldKey}) : super(key: key);

  @override
  _ContenidosFormacionState createState() => _ContenidosFormacionState();
}

class _ContenidosFormacionState extends StateMVC<ContenidosFormacion> {
  ContenidoFormacionController _con;

  _ContenidosFormacionState() : super(ContenidoFormacionController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
    _con.getContenido();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _con.scaffoldKey,
        drawer: DrawerWidget(),
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Text('Contenidos de Formación',
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 15)),
          elevation: 0,
          leading: Builder(
            builder: (BuildContext context) {
              return new IconButton(
                icon: Icon(Icons.sort, color: Colors.black),
                onPressed: () {
                  Scaffold.of(context).openDrawer();
                },
              );
            },
          ),
        ),
        body: !perfilUsuario.value.afiliado
            ? PermisosDenegados()
            : SingleChildScrollView(
                padding: EdgeInsets.symmetric(horizontal: 0, vertical: 10),
                child: Column(
                  children: [
                    // Padding(
                    //     padding: const EdgeInsets.symmetric(horizontal: 20),
                    //     child: SearchBarWidget(
                    //       onClickFilter: (event) {},
                    //     )),
                    // SizedBox(height: 20),
                    _con.contenidos.isEmpty
                        ? CircularLoadingWidget(height: 500)
                        : ListView.separated(
                            padding: EdgeInsets.symmetric(vertical: 5),
                            scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                            primary: false,
                            itemCount: _con.contenidos.length,
                            separatorBuilder: (context, index) {
                              return Divider(
                                height: 1,
                                color: Colors.grey,
                              );
                            },
                            itemBuilder: (context, index) {
                              return ListTile(
                                onTap: () {
                                  print(
                                      'INFORMACIÓN ${_con.contenidos.elementAt(index)}');
                                  Navigator.of(context).pushNamed(
                                      '/ContenidoFormacionDetalle',
                                      arguments:
                                          _con.contenidos.elementAt(index));
                                },
                                trailing: Icon(
                                  FontAwesomeIcons.plus,
                                  size: 18,
                                  color: Colors.red.withOpacity(1),
                                ),
                                title: Text(
                                  _con.contenidos.elementAt(index).titulo,
                                  style: TextStyle(
                                      fontSize: 17,
                                      fontWeight: FontWeight.w700),
                                ),
                              );
                            },
                          ),
                  ],
                )));
  }
}
