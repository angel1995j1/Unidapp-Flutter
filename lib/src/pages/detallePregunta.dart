import 'package:flutter/material.dart';
import 'package:unidapp/src/api/user_repository.dart';
import 'package:unidapp/src/elements/DrawerWidget.dart';
import 'package:unidapp/src/elements/PermisosDenegadosWidget.dart';
import 'package:unidapp/src/models/preguntas_model.dart';

class PreguntaDetalle extends StatefulWidget {
  Preguntas pregunta;

  PreguntaDetalle({Key key, this.pregunta}) : super(key: key);

  @override
  _PreguntaDetalleState createState() => _PreguntaDetalleState();
}

class _PreguntaDetalleState extends State<PreguntaDetalle> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: DrawerWidget(),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: Builder(
          builder: (BuildContext context) {
            return new IconButton(
              icon: Icon(Icons.sort, color: Theme.of(context).hintColor),
              onPressed: () {
                Scaffold.of(context).openDrawer();
              },
            );
          },
        ),
      ),
      body: !perfilUsuario.value.afiliado
          ? PermisosDenegados()
          : Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 15),
                  child: Align(
                    alignment: Alignment.topRight,
                    child: Container(
                        child: Padding(
                      padding: const EdgeInsets.only(right: 50),
                      child: Text(
                        widget.pregunta.titulo,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 16),
                      ),
                    )),
                  ),
                ),
                Divider(
                  indent: 190,
                  height: 10,
                  thickness: 2,
                  color: Colors.black,
                ),
                SizedBox(height: 20),
                Center(
                    child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 15),
                  child: Text(widget.pregunta.titulo,
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
                )),
                SizedBox(height: 30),
                Container(
                  height: MediaQuery.of(context).size.height / 2,
                  width: MediaQuery.of(context).size.width / 1.2,
                  child: Text(
                    widget.pregunta.detalles,
                    textAlign: TextAlign.justify,
                    overflow: TextOverflow.fade,
                  ),
                ),
              ],
            ), // Esta coma final hace que el formateo automático sea mejor para los métodos de compilación.
    );
  }
}
