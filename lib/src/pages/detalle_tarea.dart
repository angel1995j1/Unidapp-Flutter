import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:unidapp/src/controllers/notas_controllers.dart';
import '../models/Task.dart';

class DetalleTarea extends StatefulWidget {
  Tareas detalleTarea;

  DetalleTarea({Key key, this.detalleTarea}) : super(key: key);

  @override
  _DetalleTareaState createState() => _DetalleTareaState();
}

class _DetalleTareaState extends StateMVC<DetalleTarea> {
  NotasController _con;
  bool switched = false;
  bool checkedValue = false;

  _DetalleTareaState() : super(NotasController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _con.scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () => Navigator.of(context).pop(),
          ),
          centerTitle: false,
        ),
        body: SafeArea(
            child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(15),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                ListTile(
                  title: Align(
                    alignment: AlignmentDirectional.bottomEnd,
                    child: Text('Fecha limite:'),
                  ),
                  subtitle: Align(
                      alignment: AlignmentDirectional.bottomEnd,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Icon(Icons.calendar_today, color: Colors.red),
                          SizedBox(width: 10),
                          // Text(
                          //     DateFormat('dd-MM-yyyy | HH:mm').format(
                          //         DateTime.parse(
                          //             widget.detalleTarea.fechaLimite)),
                          //     style: TextStyle(color: Colors.red)),
                          Text(widget.detalleTarea.fechaLimite,
                              style: TextStyle(color: Colors.red)),
                        ],
                      )),
                ),
                SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(widget.detalleTarea.titulo,
                        overflow: TextOverflow.fade,
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold)),
                    Switch(
                      value: widget.detalleTarea.finalizado,
                      onChanged: (value) {
                        setState(() {
                          widget.detalleTarea.finalizado = value;
                          _con.editarTarea(widget.detalleTarea);
                        });
                      },
                      activeColor: Colors.red,
                    )
                  ],
                ),
                Divider(
                  indent: 190,
                  height: 10,
                  thickness: 2,
                  color: Colors.black,
                ),
                SizedBox(height: 30),
                Text(
                  widget.detalleTarea.tarea,
                  style: TextStyle(fontSize: 17),
                ),
                SizedBox(height: 20),
                ListView.separated(
                  padding: EdgeInsets.symmetric(vertical: 5),
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  primary: false,
                  separatorBuilder: (context, index) {
                    return SizedBox(height: 5);
                  },
                  itemCount: widget.detalleTarea.subtarea.length,
                  itemBuilder: (context, index) {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Checkbox(
                          tristate: false,
                          checkColor: Colors.white,
                          activeColor: Colors.red,
                          value: widget.detalleTarea.subtarea
                              .elementAt(index)
                              .finalizar,
                          onChanged: (newValue) {
                            setState(() {
                              widget.detalleTarea.subtarea
                                  .elementAt(index)
                                  .finalizar = newValue;
                              _con.editarTarea(widget.detalleTarea);
                            });
                          },
                        ),
                        Text(widget.detalleTarea.subtarea
                            .elementAt(index)
                            .titulo)
                      ],
                    );
                  },
                ),
                SizedBox(height: 40),
                Center(
                    child: Container(
                  height: 45,
                  width: 240,
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                          color: Colors.red.withOpacity(0.4),
                          blurRadius: 10,
                          offset: Offset(0, 7)),
                      BoxShadow(
                          color: Colors.red.withOpacity(0.2),
                          blurRadius: 5,
                          offset: Offset(0, 3))
                    ],
                    borderRadius: BorderRadius.all(Radius.circular(40)),
                  ),
                  child: RaisedButton(
                    color: Colors.red,
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(7)),
                    onPressed: () {
                      _con.tareaCompletada(widget.detalleTarea.sId);
                      _con.obtenerTareas();
                      Navigator.of(context).pop();
                    },
                    child: Text('Tarea completada',
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold)),
                  ),
                ))
              ],
            ),
          ),
        )));
  }
}
