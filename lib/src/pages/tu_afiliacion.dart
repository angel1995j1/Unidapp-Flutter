import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:unidapp/src/api/user_repository.dart';
import 'package:unidapp/src/elements/DrawerWidget.dart';
import 'package:unidapp/src/elements/PermisosDenegadosWidget.dart';

class TuSindicato extends StatefulWidget {
  TuSindicato({Key key}) : super(key: key);

  @override
  _TuSindicatoState createState() => _TuSindicatoState();
}

class _TuSindicatoState extends State<TuSindicato> {
  final Uri _emailLaunchUri = Uri(
      scheme: 'mailto',
      path: 'unidappcolombia@gmail.com',
      queryParameters: {'subject': 'Hola!'});
  _launchURL() async {
    const url =
        'https://videounidapp.fra1.digitaloceanspaces.com/ESTATUTOS%20UNIDAPP%20%20%281%29.pdf';
    if (await canLaunch(url)) {
      await launch(url);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: DrawerWidget(),
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: Builder(
            builder: (BuildContext context) {
              return new IconButton(
                icon: Icon(Icons.sort, color: Theme.of(context).hintColor),
                onPressed: () {
                  Scaffold.of(context).openDrawer();
                },
              );
            },
          ),
        ),
        body: !perfilUsuario.value.afiliado
            ? PermisosDenegados()
            : SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Align(
                      alignment: Alignment.topRight,
                      child: Container(
                          child: Padding(
                        padding: const EdgeInsets.only(right: 50),
                        child: Text(
                          "Conoce tu Sindicato",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 16),
                        ),
                      )),
                    ),
                    Divider(
                      indent: 190,
                      height: 10,
                      thickness: 2,
                      color: Colors.black,
                    ),
                    SizedBox(height: 10),
                    Center(
                        child: Text('Tu Sindicato',
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold))),
                    Padding(
                      padding: EdgeInsets.all(20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          RichText(
                            softWrap: true,
                            textAlign: TextAlign.justify,
                            text: TextSpan(
                                text:
                                    'Es una organización sindical conformada por trabajadores y trabajadoras de plataformas en general, cuyas funciones se caracterizan por ser domiciliario, riders, repartidor, mensajero, auxiliar de compras (shoppers), conductor de taxi, paseador de perro, personal de soporte. Nosotros velaremos por que se nos garantice el derecho a ser reconocidos como trabajadores y trabajadoras:  en consecuencia, poder acceder al trabajo digno con un ingreso justo y protección social.  La Unión de Trabajadores de Plataformas garantiza una asesoría legal para aquellos trabajadores y trabajadoras a quienes les vulneren sus derechos.',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 15,
                                    fontWeight: FontWeight.w400)),
                          ),
                          SizedBox(height: 30),
                          Text('Beneficios de pertenecer a UNIDAPP',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 16)),
                          SizedBox(height: 30),
                          RichText(
                              softWrap: true,
                              textAlign: TextAlign.justify,
                              text: TextSpan(
                                  text: 'A. Condiciones de trabajo: ',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black,
                                      fontSize: 16),
                                  children: <TextSpan>[
                                    TextSpan(
                                        text:
                                            'por medio del proceso que iniciamos como sindicato nuestro propósito es usar nuestra fuerza colectiva para negociar mejores salarios, tarifas en cuanto a los pedidos o servicios que prestamos independientemente a la plataforma digital a la que pertenecemos, poder obtener el beneficio de pensión, jubilación, vacaciones, seguro de salud, pago por enfermedad, horas extras, seguro de maternidad, como sindicato realizar este tipo de negociaciones para acabar con la precariedad laboral de trabajador de plataformas digitales.',
                                        style: TextStyle(
                                            fontSize: 15,
                                            color: Colors.black,
                                            fontWeight: FontWeight.w400))
                                  ])),
                          SizedBox(height: 20),
                          RichText(
                              softWrap: true,
                              textAlign: TextAlign.justify,
                              text: TextSpan(
                                  text: 'B. Protección personal: ',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black,
                                      fontSize: 16),
                                  children: <TextSpan>[
                                    TextSpan(
                                        text:
                                            'Defender los derechos de los trabajadores de plataformas digitales, por mal trato, acoso, persecución por parte de patronos, clientes o abuso de poder de algún ente gubernamental que traten injustamente, apoyando en la capacitación, aprendizaje y protección social y adaptarnos en el proceso de transición al nuevo mundo laboral.',
                                        style: TextStyle(
                                            fontSize: 15,
                                            color: Colors.black,
                                            fontWeight: FontWeight.w400))
                                  ])),
                          SizedBox(height: 20),
                          RichText(
                              softWrap: true,
                              textAlign: TextAlign.justify,
                              text: TextSpan(
                                  text: 'C. Igualdad: ',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black,
                                      fontSize: 16),
                                  children: <TextSpan>[
                                    TextSpan(
                                        text:
                                            'El derecho al mismo ingreso equitativamente a todos los trabajadores, hombres, mujeres, blancos, negros, indígenas, mestizos, heterosexual, homosexual, fomentando el respeto y la dignidad en el lugar de trabajo, trabajo flexible, derecho a la maternidad y el pago por paternidad donde se pueda compartir las responsabilidades del cuidado, capacitar a los padres por medio de los programas estatales del cuidado de los hijos, mientras padre y madres trabajan. ',
                                        style: TextStyle(
                                            fontSize: 15,
                                            color: Colors.black,
                                            fontWeight: FontWeight.w400))
                                  ])),
                          SizedBox(height: 20),
                          RichText(
                              softWrap: true,
                              textAlign: TextAlign.justify,
                              text: TextSpan(
                                  text: 'D. Salud y seguridad: ',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black,
                                      fontSize: 16),
                                  children: <TextSpan>[
                                    TextSpan(
                                        text:
                                            'se ha comprobado que muchas empresas de plataformas digitales no dan ningún tipo de cuidado a la integridad física de un trabajador de plataforma digital donde esto corre por cuenta de trabajador, los cuales arriesgamos nuestras vida. Por eso trabajamos en que se nos reconozca la protección en salud y seguridad, rechazamos el trabajo peligroso, tomando en cuenta los pesos que se trasladan en las maletas de los repartidores y de los auxiliares de compras, las zonas donde nos dirigimos que pueden ser peligrosos por ser víctimas de robo, seguridad vial, por acoso por parte de un cliente o ciudadano común donde se establezca una red de seguridad para comunicarnos. Entre otros.',
                                        style: TextStyle(
                                            fontSize: 15,
                                            color: Colors.black,
                                            fontWeight: FontWeight.w400))
                                  ])),
                          SizedBox(height: 20),
                          RichText(
                              softWrap: true,
                              textAlign: TextAlign.justify,
                              text: TextSpan(
                                  text: 'E. Solidaridad: ',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black,
                                      fontSize: 16),
                                  children: <TextSpan>[
                                    TextSpan(
                                        text:
                                            'Contamos con una red de apoyo conformada por organizaciones no gubernamentales y el apoyo de la Central Unitaria de Trabajadores (CUT), Unión Sindical Obrera (USO), y demás organizaciones sindicales afiliadas a la (CUT). Somos una gran familia sindical en defensa de los trabajadores y trabajadoras de plataformas digitales en el mundo.',
                                        style: TextStyle(
                                            fontSize: 15,
                                            color: Colors.black,
                                            fontWeight: FontWeight.w400))
                                  ])),
                          SizedBox(height: 20),
                          RichText(
                              softWrap: true,
                              textAlign: TextAlign.justify,
                              text: TextSpan(
                                  text: 'Junta directiva',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold))),
                          SizedBox(height: 20),
                          RichText(
                              softWrap: true,
                              textAlign: TextAlign.justify,
                              text: TextSpan(
                                  text: 'Presidente: ',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16),
                                  children: <TextSpan>[
                                    TextSpan(
                                        text: '  Álvaro Andrés Barbosa Ramírez',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w400))
                                  ])),
                          SizedBox(
                            height: 10,
                          ),
                          RichText(
                              softWrap: true,
                              text: TextSpan(
                                  text: 'Vicepresidente: ',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16),
                                  children: <TextSpan>[
                                    TextSpan(
                                        text: '  Sergio Andrés 	Solano Sánchez',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w400))
                                  ])),
                          SizedBox(
                            height: 10,
                          ),
                          RichText(
                              softWrap: true,
                              text: TextSpan(
                                  text: 'Secretaría General: ',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16),
                                  children: <TextSpan>[
                                    TextSpan(
                                        text:
                                            '  Carolina Del Valle Hevia de Brandts',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w400))
                                  ])),
                          SizedBox(
                            height: 10,
                          ),
                          RichText(
                              softWrap: true,
                              text: TextSpan(
                                  text: 'Tesorería: ',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16),
                                  children: <TextSpan>[
                                    TextSpan(
                                        text: '  Juan Carlos Uzcátegui Salas',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w400))
                                  ])),
                          SizedBox(
                            height: 10,
                          ),
                          RichText(
                              softWrap: true,
                              text: TextSpan(
                                  text: 'Fiscal: ',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16),
                                  children: <TextSpan>[
                                    TextSpan(
                                        text: '  Jonathan Reyes',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w400))
                                  ])),
                          SizedBox(
                            height: 10,
                          ),
                          RichText(
                              softWrap: true,
                              text: TextSpan(
                                  text:
                                      'Secretaría educación y participación: ',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16),
                                  children: <TextSpan>[
                                    TextSpan(
                                        text: '  Juan Carlos López',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w400))
                                  ])),
                          SizedBox(
                            height: 10,
                          ),
                          RichText(
                              softWrap: true,
                              text: TextSpan(
                                  text:
                                      'Secretaría de género y enfoque diferencial: ',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16),
                                  children: <TextSpan>[
                                    TextSpan(
                                        text: ' Andreina Karelis Pérez Márquez',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w400))
                                  ])),
                          SizedBox(
                            height: 10,
                          ),
                          RichText(
                              softWrap: true,
                              text: TextSpan(
                                  text: 'Secretaría de comunicaciones: ',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16),
                                  children: <TextSpan>[
                                    TextSpan(
                                        text: ' Leandro Rafael Ochoa Melean',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w400))
                                  ])),
                          SizedBox(
                            height: 10,
                          ),
                          RichText(
                              softWrap: true,
                              text: TextSpan(
                                  text: 'Secretaría de juventudes: ',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16),
                                  children: <TextSpan>[
                                    TextSpan(
                                        text:
                                            ' Leonardo José	Gutiérrez Ramírez',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w400))
                                  ])),
                          SizedBox(
                            height: 10,
                          ),
                          RichText(
                              softWrap: true,
                              text: TextSpan(
                                  text: 'Secretaría migración: ',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16),
                                  children: <TextSpan>[
                                    TextSpan(
                                        text: ' Fabián Timoteo - Fermín Núñez',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w400))
                                  ])),
                          SizedBox(
                            height: 10,
                          ),
                          GestureDetector(
                            onTap: () {
                              _launchURL();
                            },
                            child: Text(
                              'Ver estatutos de unión de trabajadores de plataformas “UNIDAPP”',
                              style: TextStyle(
                                  color: Colors.red,
                                  fontWeight: FontWeight.bold,
                                  decoration: TextDecoration.underline,
                                  fontSize: 16),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(height: 10),
                    Container(
                        height: 180,
                        width: 290,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          image: DecorationImage(
                            image: AssetImage('assets/img/tusindicato.jpeg'),
                            fit: BoxFit.fill,
                          ),
                          shape: BoxShape.rectangle,
                        )),
                    SizedBox(height: 25),
                    Padding(
                      padding: const EdgeInsets.only(right: 60),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          IconButton(
                            icon: Icon(FontAwesomeIcons.twitter,
                                size: 25, color: Colors.blue),
                            onPressed: () {
                              launch('https://twitter.com/UNIDAPPCOL');
                            },
                          ),
                          IconButton(
                            icon: Icon(FontAwesomeIcons.facebookSquare,
                                size: 25, color: Colors.blue[900]),
                            onPressed: () {
                              launch(
                                  'https://www.facebook.com/groups/381858842848494');
                            },
                          ),
                          IconButton(
                            icon: Icon(FontAwesomeIcons.google,
                                size: 25, color: Color(0xFFE24134)),
                            onPressed: () {
                              launch(_emailLaunchUri.toString());
                            },
                          ),
                          IconButton(
                            icon: Icon(FontAwesomeIcons.instagram,
                                size: 25, color: Color(0xFF7B46B8)),
                            onPressed: () {
                              launch(
                                  'https://www.instagram.com/unidapp_col?r=nametag');
                            },
                          ),
                          IconButton(
                            icon: Icon(FontAwesomeIcons.youtube,
                                size: 25, color: Colors.red[700]),
                            onPressed: () {
                              launch(
                                  'https://youtube.com/channel/UCOxP0yqYopjDtj7nuHSbIcg');
                            },
                          ),
                          IconButton(
                            icon: Icon(FontAwesomeIcons.tiktok,
                                size: 25, color: Colors.black),
                            onPressed: () {
                              launch('https://vm.tiktok.com/ZMebyG5Sy/');
                            },
                          )
                        ],
                      ),
                    ),
                    SizedBox(height: 40),
                  ],
                ),
              ));
  }
}
