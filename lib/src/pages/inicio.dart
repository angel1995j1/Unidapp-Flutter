import 'dart:typed_data';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:slider_button/slider_button.dart';
import 'package:unidapp/src/api/user_repository.dart';
import 'package:unidapp/src/controllers/home_controller.dart';
import 'package:unidapp/src/elements/CircularLoadingWidget.dart';
import 'package:unidapp/src/elements/DrawerWidget.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:unidapp/src/models/route_argument.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends StateMVC<HomePage> {
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

  HomeController _con;

  _HomePageState() : super(HomeController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
    var androidInitialize = new AndroidInitializationSettings('app_icon');
    var iOSInitialize = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(
        android: androidInitialize, iOS: iOSInitialize);

    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
    flutterLocalNotificationsPlugin.initialize(initializationSettings);
    _checkDynamicLinks();
  }

  _checkDynamicLinks() async {
    FirebaseDynamicLinks.instance.onLink(
        onSuccess: (PendingDynamicLinkData dynamicLink) async {
      final Uri deepLink = dynamicLink?.link;

      if (deepLink != null) {
        // Navigator.pushNamed(context, deepLink.path);
        print('OBTENIENDO DEEPLINK ${deepLink.queryParameters}');
        Navigator.of(context).pushNamed('/UnirseReunion',
            arguments: RouteArgument(param: deepLink.queryParameters));
      }
    }, onError: (OnLinkErrorException e) async {
      print('onLinkError');
      print(e.message);
    });
  }

  Future<void> showPeriodicalNotification(String titulo, String body) async {
    const AndroidNotificationDetails androidPlatformChannelSpecifics =
        AndroidNotificationDetails('repeating channel id',
            'repeating channel name', 'repeating description');
    const NotificationDetails platformChannelSpecifics =
        NotificationDetails(android: androidPlatformChannelSpecifics);

    await flutterLocalNotificationsPlugin.periodicallyShow(
        0, titulo, body, RepeatInterval.daily, platformChannelSpecifics,
        androidAllowWhileIdle: true);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      drawer: DrawerWidget(),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        title: Text('Inicio', style: TextStyle(color: Colors.black)),
        centerTitle: true,
        elevation: 0,
        leading: Builder(
          builder: (BuildContext context) {
            return new IconButton(
              icon: Icon(Icons.sort, color: Theme.of(context).hintColor),
              onPressed: () {
                !perfilUsuario.value.pagado
                    ? showPeriodicalNotification('Pago', 'Recordatorio de pago')
                    : print('Ya pago el puñetas');
                _con.havePendigTask
                    ? showPeriodicalNotification(
                        'Tareas', 'Tienes tareas pendientes')
                    : print('El usuario tiene tareas pendientes');
                Scaffold.of(context).openDrawer();
              },
            );
          },
        ),
      ),
      body: SingleChildScrollView(
        child: _con.loading
            ? CircularLoadingWidget(height: 100)
            : Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Center(
                    child: Image.asset('./assets/img/Unilogo.png', width: 140),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 15),
                  AutoSizeText(
                    'Tus enlaces rápidos'.toUpperCase(),
                    minFontSize: 17,
                    maxFontSize: 19,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 15),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: MediaQuery.of(context).size.height / 10,
                        width: MediaQuery.of(context).size.width / 3,
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                                color: Colors.red.withOpacity(0.4),
                                blurRadius: 10,
                                offset: Offset(0, 7)),
                            BoxShadow(
                                color: Colors.red.withOpacity(0.2),
                                blurRadius: 5,
                                offset: Offset(0, 3))
                          ],
                          borderRadius: BorderRadius.all(Radius.circular(40)),
                        ),
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(7)),
                          color: Theme.of(context).accentColor,
                          child: AutoSizeText(
                            'Preguntas Frecuentes'.toUpperCase(),
                            minFontSize: 12,
                            maxFontSize: 13,
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                            textAlign: TextAlign.center,
                          ),
                          onPressed: () {
                            Navigator.of(context)
                                .pushNamed('/PreguntasFrecuentes');
                          },
                        ),
                      ),
                      const SizedBox(width: 20),
                      Container(
                        height: MediaQuery.of(context).size.height / 10,
                        width: MediaQuery.of(context).size.width / 3,
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                                color: Colors.red.withOpacity(0.4),
                                blurRadius: 10,
                                offset: Offset(0, 7)),
                            BoxShadow(
                                color: Colors.red.withOpacity(0.2),
                                blurRadius: 5,
                                offset: Offset(0, 3))
                          ],
                          borderRadius: BorderRadius.all(Radius.circular(40)),
                        ),
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(7)),
                          child: AutoSizeText('Asesoría jurídica'.toUpperCase(),
                              minFontSize: 12,
                              maxFontSize: 13,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold)),
                          color: Theme.of(context).accentColor,
                          onPressed: () {
                            Navigator.of(context)
                                .pushNamed('/AsesoriasJuridicas');
                          },
                        ),
                      )
                    ],
                  ),
                  const SizedBox(height: 30),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                          height: MediaQuery.of(context).size.height / 10,
                          width: MediaQuery.of(context).size.width / 3,
                          decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.red.withOpacity(0.4),
                                  blurRadius: 10,
                                  offset: Offset(0, 7)),
                              BoxShadow(
                                  color: Colors.red.withOpacity(0.2),
                                  blurRadius: 5,
                                  offset: Offset(0, 3))
                            ],
                            borderRadius: BorderRadius.all(Radius.circular(40)),
                          ),
                          child: RaisedButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(7)),
                            color: Theme.of(context).accentColor,
                            child: AutoSizeText(
                              'Contenidos de formación'.toUpperCase(),
                              minFontSize: 12,
                              maxFontSize: 13,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            onPressed: () {
                              Navigator.of(context)
                                  .pushNamed('/ContenidoFormacion');
                            },
                          )),
                      SizedBox(width: 20),
                      Container(
                        height: MediaQuery.of(context).size.height / 10,
                        width: MediaQuery.of(context).size.width / 3,
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                                color: Colors.red.withOpacity(0.4),
                                blurRadius: 10,
                                offset: Offset(0, 7)),
                            BoxShadow(
                                color: Colors.red.withOpacity(0.2),
                                blurRadius: 5,
                                offset: Offset(0, 3))
                          ],
                          borderRadius: BorderRadius.all(Radius.circular(40)),
                        ),
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(7)),
                          child: AutoSizeText(
                            'Tu sindicado'.toUpperCase(),
                            minFontSize: 12,
                            maxFontSize: 13,
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                          color: Theme.of(context).accentColor,
                          onPressed: () {
                            Navigator.of(context).pushNamed('/TuSindicato');
                          },
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 17),
                  perfilUsuario.value.pagado && perfilUsuario.value.afiliado
                      ? Container()
                      : Container(
                          alignment: Alignment.bottomRight,
                          padding: EdgeInsets.all(10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Icon(FontAwesomeIcons.clock,
                                  color: Colors.red, size: 19),
                              SizedBox(width: 10),
                              AutoSizeText('Por vencer',
                                  minFontSize: 12,
                                  maxFontSize: 13,
                                  style: TextStyle(fontWeight: FontWeight.bold))
                            ],
                          ),
                        ),
                  perfilUsuario.value.pagado && perfilUsuario.value.afiliado
                      ? Container()
                      : Divider(
                          indent: 190,
                          height: 10,
                          thickness: 2,
                          color: Colors.black,
                        ),
                  const SizedBox(height: 30),
                  !perfilUsuario.value.afiliado
                      ? Container()
                      : Column(
                          children: [
                            perfilUsuario.value.pagado
                                ? Container()
                                : Container(
                                    child: Column(
                                      children: [
                                        Text(
                                            'Tienes un monto pendiente de cuota sindical por \$2.000.00',
                                            style: TextStyle(fontSize: 13)),
                                        SizedBox(height: 10),
                                        Container(
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(40)),
                                          ),
                                          child: RaisedButton(
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  new BorderRadius.circular(
                                                7,
                                              ),
                                            ),
                                            child: Text(
                                              'IR A PAGAR',
                                              style: TextStyle(
                                                color: Colors.white,
                                              ),
                                            ),
                                            color:
                                                Theme.of(context).accentColor,
                                            onPressed: () {
                                              Navigator.of(context)
                                                  .pushNamed('/PaymentMethod');
                                            },
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                            const SizedBox(height: 20),
                            _con.havePendigTask
                                ? Center(
                                    child: SliderButton(
                                      action: () {
                                        ///Do something here OnSlide
                                      },
                                      label: Text(
                                        "¡TAREA PENDIENTE!",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 14),
                                      ),
                                      icon: Center(
                                          child: Icon(
                                        Icons.power_settings_new,
                                        color: Colors.white,
                                        size: 40.0,
                                        semanticLabel:
                                            'Text to announce in accessibility modes',
                                      )),
                                      width: 240,
                                      radius: 30,
                                      buttonColor:
                                          Theme.of(context).accentColor,
                                      backgroundColor:
                                          Colors.grey.withOpacity(0.3),
                                      highlightedColor: Colors.white,
                                      baseColor: Colors.black,
                                    ),
                                  )
                                : Container(),
                            const SizedBox(height: 20),
                          ],
                        )
                ],
              ),
      ),
    );
  }
}
