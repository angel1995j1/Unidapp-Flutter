import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:unidapp/src/api/user_repository.dart';
import 'package:unidapp/src/models/Task.dart';
import 'package:unidapp/src/utils/helpers.dart';
import 'package:logger/logger.dart';

var dio = Dio();

var logger = Logger(
  printer: PrettyPrinter(),
);

Future obtenerTareas() async {
  Uri uri = Helper.getUri('tareas-usuario/${currentUser.value.usuario.email}');
  try {
    Response response = await dio.get(
      uri.toString(),
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status < 500;
        },
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer ${currentUser.value.token}"
        },
      ),
    );
    // logger.i('RESPONSE API DATA TASK ${response.data}');
    return Tasks.fromJson(response.data);
  } catch (e) {
    print('error $e');
    return e;
  }
}

Future<bool> tareaCompletada(id) async {
  final String url =
      '${GlobalConfiguration().getValue('api_base_url')}eliminar-tarea/$id';
  try {
    Response response = await dio.delete(url,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            },
            headers: {
              HttpHeaders.contentTypeHeader: "application/json",
              HttpHeaders.authorizationHeader:
                  "Bearer ${currentUser.value.token}"
            }));
    return true;
  } catch (e) {
    print(e);
    return false;
  }
}

Future<bool> editarTarea(dynamic tarea) async {
  final String url =
      '${GlobalConfiguration().getValue('api_base_url')}editar-tarea/${tarea.sId}';
  try {
    Response response = await dio.put(url,
        data: json.decode(json.encode(tarea)),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            },
            headers: {
              HttpHeaders.contentTypeHeader: "application/json",
              HttpHeaders.authorizationHeader:
                  "Bearer ${currentUser.value.token}"
            }));
    if (response.statusCode == 200) {
      return true;
    }
    return true;
  } catch (e) {
    print('error $e');
    return false;
  }
}
